from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render,redirect
from django.views import View
from django.contrib.auth.models import User,Group,Permission
from .forms import SignupForm
from django.views.generic.edit import DeleteView
from django.urls import reverse_lazy


UserGroup, created = Group.objects.get_or_create(name='Users')
AdminGroup, created = Group.objects.get_or_create(name='Admins')



class Signup(View):
    model = User
    template_name = 'signup.html'
    def get(self,request):
        form =SignupForm()
        context ={'form' : form}

        return render(request,self.template_name, context)


    def post(self,request):
        form = SignupForm(request.POST)

        if form.is_valid():
            user=form.save()
            password=form.cleaned_data.get('password')
            user.set_password(password)
            user.save()
            return redirect('login')


@login_required
def home(request):
    user = User.objects.all()
    user1 = User.objects.get(username=request.user)

    if user1.groups.get().id == Group.objects.get(name="Admins").id:
        return render(request,'home.html',{'user': user,'user1': user1})
    #
    else:
        return render(request,'home.html',{'user':user,'user1':user1})




class UserDelete(DeleteView):
    model = User
    success_url = reverse_lazy('home')



def edit(request):
    return render(request,'home.html')






