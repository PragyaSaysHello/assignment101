from django.urls import path
from .views import Signup,home,edit, UserDelete

urlpatterns = [
    path('', Signup.as_view(),name='signup'),
    path('home/',home, name='home'),
    path('home/<int:pk>/delete',UserDelete.as_view(), name='delete'),
    path('edit/',edit, name='edit'),
]

