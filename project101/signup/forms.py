from django import forms
from django.contrib.auth.models import User


class SignupForm(forms.ModelForm):

    class Meta:
        model=User
        fields = '__all__'
        # fields = ["first_name","last_name","username","email","password"]
        widgets = {
            'password': forms.PasswordInput()
        }
